
let softdrinks = [
    {
        "name": "Coke",
        "prize": "2$",
        "id": 1,
        "imageUrl": "https://farm1.staticflickr.com/71/203324363_b448827eb0.jpg",
        "volume": "50cl"
    },
    {
        "name": "Fanta",
        "prize": "2$",
        "id": 2,
        "imageUrl": "https://farm1.staticflickr.com/684/32876893826_130576f75a.jpg",
        "volume": "50cl"
    },
    {
        "name": "Pepsi",
        "prize": "2$",
        "id": 3,
        "imageUrl": "https://farm4.staticflickr.com/3344/3593103557_bf47c0a3a2.jpg",
        "volume": "50cl"
    },
    {
        "name": "Red bull",
        "prize": "3$",
        "id": 4,
        "imageUrl": "https://farm3.staticflickr.com/2391/2507916617_254348d40c.jpg",
        "volume": "50cl"
    }
];

softdrinks.forEach(function (softdrink) {
    let softdrinksDiv = document.createElement('div');
    softdrinksDiv.classList.add('softdrinkItem'); // Fügen Sie eine Klasse für die Flexbox hinzu
    softdrinksDiv.innerHTML =
        '<img class="imgSizeFood" src="' + softdrink.imageUrl + '">' +
        '<div class="softdrinkDetails">' + // Container für Name und Preis
            '<h5>' + softdrink.name + '</h5>' +
            '<h5>' + softdrink.prize + '</h5>' +
        '</div>' +
        '<select name="size">' +
            '<option value="330ml">330 ml</option>' +
            '<option value="500ml">500 ml</option>' +
            '<option value="1l">1 L</option>' +
        '</select>' +
        '<button onclick="incrementValue()"><img class="shoppingcartSize" src="Bilder/ShoppingCart.png" alt="Shoppingcart"></button>';

    document.getElementById("softdrinksContainer").appendChild(softdrinksDiv);
});