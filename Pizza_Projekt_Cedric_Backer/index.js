number = document.getElementById("counter").innerHTML;

function incrementValue()
{
    number++;
    counter.textContent = number;
}

function saveCounterValue(value) {
    localStorage.setItem('counter', value);
}
function getCounterValue() {
    return parseInt(localStorage.getItem('counter')) || 0; 
}
function incrementValue() {
    let number = getCounterValue();
    number++;
    saveCounterValue(number);
    document.getElementById("counter").textContent = number;
}
document.addEventListener('DOMContentLoaded', function () {
    let number = getCounterValue();
    document.getElementById("counter").textContent = number;
});




var shoppingcartAlertButton = document.getElementById("shoppingcartAlert");

shoppingcartAlertButton.addEventListener("click", function() {
    // Setzen den Counter-Wert auf 0
    saveCounterValue(0);
    
    var counterElement = document.getElementById("counter");
    var message = "Thank you for your order!";
    
    // Alert-Nachricht anzeigen
    alert(message);
    
    // Seite neu laden
    window.location.reload();
});






